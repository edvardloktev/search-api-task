Feature: Search for the product


  Scenario Outline: User can see search results
    When user call search api with <product>
    And  user sees the results displayed for <product>

    Examples:
      | product |
      | apple   |

    # Search broken for this keywords, back-end response not fully valid
     # | orange  |
     # | pasta   |
     # | cola    |

  Scenario: User getting error for not existing search keywords
    When user call search api with "car"
    Then user is not able to search incorrect product
