package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.common.mapper.TypeRef;
import net.serenitybdd.rest.SerenityRest;
import starter.rest.api.SearchAPI;
import starter.rest.models.SearchResult;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.hamcrest.Matchers.*;

public class SearchStep {

    private final SearchAPI searchAPI = new SearchAPI();

    @When("^user call search api with (.*)$")
    public void userCallsSearchEndpoint(String searchParam) {
        searchAPI.searchItems(searchParam);
    }

    @Then("^user sees the results displayed for (.*)$")
    public void userSeesTheResultsDisplayed(String searchParam) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("size()", greaterThan(0)));
        List<SearchResult> results = SerenityRest.lastResponse().body().as(new TypeRef<List<SearchResult>>() {
        });
        assertThat(results).allMatch(item -> item.getTitle().toLowerCase().contains(searchParam.toLowerCase()));
    }

    @Then("user is not able to search incorrect product")
    public void userIsNotAbleToSearchIncorrectProduct() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.body("detail.error", is(true)));
        restAssuredThat(response -> response.body("detail.message", containsStringIgnoringCase("not found")));
    }
}
