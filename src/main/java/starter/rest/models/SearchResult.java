package starter.rest.models;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Jacksonized
@Builder
public class SearchResult {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private String price;
    private String unit;
    private boolean isPromo;
    private String promoDetails;
    private String image;
}
