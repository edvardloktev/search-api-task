package starter.rest.api;

import net.serenitybdd.rest.SerenityRest;

public class SearchAPI extends BaseAPI {

    private final String searchURL = baseUrl + "/api/v1/search/demo/%s";

    public void searchItems(String searchItem) {
        SerenityRest.given().get(String.format(searchURL, searchItem));
    }
}
