package starter.rest.api;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class BaseAPI {

    private static final EnvironmentVariables environmentVariables = Injectors.getInjector().getInstance(EnvironmentVariables.class);
    protected final String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
            .getProperty("restapi.baseurl");
}
